from rest_framework import serializers
from .models import Article, Category, Tag

# 普通序列化
# class ArticleSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     title = serializers.CharField(max_length=100, required=True)
#     vum = serializers.IntegerField(required=True)
#     content = serializers.CharField()
#
#     def create(self, validated_data):
#         return Article.objects.create(**validated_data)
#
#     def update(self, instance, validated_data):
#         instance.title = validated_data.get('title', instance.title)
#         instance.vum = validated_data.get('vum', instance.vum)
#         instance.content = validated_data.get('title', instance.content)
#         instance.save()
#         return instance

"""
# 序列化
from app02.serializers import ArticleSerializer
from rest_framework.renderers import JSONRenderer
>>> d = {'title':'文章标题'}
>>> ret = ArticleSerializer(data=d)
>>> ret.is_valid()
>>> False

>>> d = {'title':'文章标题','vum':100, 'content':'文章内容'}
>>> ret =ArticleSerializer(data=d)
>>> ret.is_valid()
>>> ret.save()
<Article: 文章标题>

# 反序列化
>>> ret =  Article.objects.all()
>>> ser = ArticleSerializer(instance=ret, many=True)  # 多个数据需要加  many=True
>>> from rest_framework.renderers import JSONRenderer
>>> json_data = JSONRenderer().render(ser.data)
>>> json_data
b'[{"id":1,"title":"\xe6\x96\x87\xe7\xab\xa0\xe6\xa0\x87\xe9\xa2\x98","vum":100,"content":"\xe6\x96\x87\xe7\xab\xa0\xe5\x86\x85\xe5\xae\xb9"}]'

>>> ret =  Article.objects.get(id=1)
>>> ser = ArticleSerializer(instance=ret)     #单个数据不需要加 many=True
>>> json_data = JSONRenderer().render(ser.data)
>>> json_data
b'{"id":1,"title":"\xe6\x96\x87\xe7\xab\xa0\xe6\xa0\x87\xe9\xa2\x98","vum":100,"content":"\xe6\x96\x87\xe7\xab\xa0\xe5\x86\x85\xe5\xae\xb9"}'

当反序列化的时候，会帮我们校验数据是否合法
当序列多个数据的时候  需要加  many=True, 单个对象不需要
当我们序列化的时候，用关键字 instance 或者不用
当我们反序列化的时候, 要用data关键字
 
"""

"""
    模型序列化
    
"""
# class ArticleSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Article
#         # fields = ('id', 'title', 'vnum', 'content')  # 三者取一
#         # exclude = () 表示不返回字段        三者取一
#         fields = '__all__'  # 表示所有字段       三者取一
#         # read_only_fields = () #设置只读字段 不接受用户修改

#
# class CategorySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Category
#         fields = '__all__'
#

"""

1 .StringRelatedField
    def __str__(self):
        return self.name
        
    此字段将被序列化为关联对象的字符串表示方式（即__str__方法的返回值）   
    {
        "id": 3,
        "category": 分类二,
        "title": "111111文章标题",
        "vum": 11100,
        "content": "11111文章内容",
    }

"""

# class ArticleSerializer(serializers.ModelSerializer):
#     category = serializers.StringRelatedField()
#
#     class Meta:
#         model = Article
#         fields = '__all__'
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     articles = serializers.StringRelatedField(many=True)
#
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'articles')


"""
    PrimaryKeyRelatedField
    此字段将被序列化为关联对象的主键。/ 默认也是显示主键
    {
        "id": 1,
        "name": "游戏",
        "articles": [
            1,
            2,
            4
        ]
    }
"""

# class ArticleSerializer(serializers.ModelSerializer):
#     category = serializers.PrimaryKeyRelatedField(read_only=True)
#
#     class Meta:
#         model = Article
#         fields = '__all__'
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     articles = serializers.PrimaryKeyRelatedField(read_only=True, many=True)
#
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'articles')

"""
 HyperlinkedRelatedField
    返回超链接
    {
    "id": 1,
    "name": "游戏",
        "articles": [
        "http://127.0.0.1:8003/app02/articles/1/",
        "http://127.0.0.1:8003/app02/articles/2/",
        "http://127.0.0.1:8003/app02/articles/4/"
        ]
    }
"""

# class ArticleSerializer(serializers.ModelSerializer):
#     category = serializers.HyperlinkedRelatedField(
#         view_name='app02:category-detail',  # 根级别名 and  路由别名
#         read_only=True,  #
#         # lookup_field='pk'
#     )
#
#     class Meta:
#         model = Article
#         fields = '__all__'
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     articles = serializers.HyperlinkedRelatedField(
#         view_name='app02:article-detail',
#         read_only=True,
#         many=True,  # 很重要
#         # lookup_field='pk'
#
#     )
#
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'articles')

"""
    SlugRelatedField
    返回一个字段
    {
        "id": 1,
        "name": "游戏",
        "articles": [
            "文章内容",
            "11111文章内容",
            "11111文章内容"
        ]
    }
"""

# class ArticleSerializer(serializers.ModelSerializer):
#     category = serializers.SlugRelatedField(
#         read_only=True,
#         slug_field='name'
#     )
#
#     class Meta:
#         model = Article
#         fields = '__all__'
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     articles = serializers.SlugRelatedField(
#         read_only=True,
#         slug_field='content',
#         many=True
#     )
#
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'articles')


"""

HyperlinkedIdentityField

    使用 HyperlinkedIdentityField 将返回指定 view-name 的超链接的字段。
    
    - view_name 应该用作关系目标的视图名称。如果您使用的是标准路由器类，则它将是格式为 <model_name>-detail的字符串。必选参数
    - lookup_field 应该用于查找的目标上的字段。应该对应于引用视图上的 URL 关键字参数。默认值为 pk
    - lookup_url_kwarg 与查找字段对应的 URL conf 中定义的关键字参数的名称。默认使用与 lookup_field 相同的值
    - format 如果使用 format 后缀，超链接字段将对目标使用相同的 format 后缀，除非使用 format 参数进行覆盖

"""

# class ArticleSerializer(serializers.ModelSerializer):
#     category = serializers.HyperlinkedIdentityField(
#         view_name='app02:category-detail',
#     )
#
#     class Meta:
#         model = Article
#         fields = '__all__'
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     articles = serializers.HyperlinkedIdentityField(
#         view_name='app02:article-detail',
#         many=True,
#     )
#
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'articles')

#

"""
HyperlinkedModelSerializer

`HyperlinkedModelSerializer` 类与 `ModelSerializer` 类相似，只不过它使用超链接来表示关系而不是主键。


{
    "id": 1,
    "name": "游戏",
    "articles": [
        "http://127.0.0.1:8003/app02/articles/1/",
        "http://127.0.0.1:8003/app02/articles/2/",
        "http://127.0.0.1:8003/app02/articles/4/"
    ],
    "url": "http://127.0.0.1:8003/app02/categorys/1/"
}
"""

#
# class ArticleSerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Article
#         fields = '__all__'
#
#         extra_kwargs = {
#             'url': {'view_name': 'app02:article-detail', 'lookup_field': 'pk'},
#             'category': {'view_name': 'app02:category-detail', 'lookup_field': 'pk'},
#         }
#
#
# class CategorySerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'articles', 'url')
#
#         extra_kwargs = {
#             'url': {'view_name': 'app02:category-detail', 'lookup_field': 'pk'},
#             'articles': {'view_name': 'app02:article-detail', 'lookup_field': 'pk'},
#         }


"""
序列嵌套 ********************************************************************************************************

{
    "id": 1,
    "name": "游戏",
    "articles": [
        {
            "id": 1,
            "title": "文章标题",
            "vum": 100,
            "content": "文章内容",
            "category": 1,
            "tags": []
        },
    ]
}
"""

# class ArticleSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Article
#         fields = '__all__'
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     articles = ArticleSerializer(many=True)  # ***********指定序列化
#
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'articles')

""""
depth
只支持序列化/不支持反序列化/ 
"""

#
# class ArticleSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Article
#         fields = '__all__'
#         depth = 2
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'articles')
#         depth = 1  # 建议写到2-3

""""
SerializerMethodField


{
    "id": 1,
    "name": "游戏",
    "articles": [  
        {
        "id": 4,
        "title": "111111文章标题",
        "vum": 11100,
        "content": "11111文章内容",
        "category": 1,
        "tags": []
        }
        ,...............
    ],
    "count": 3
}

"""


class TagSerializer(serializers.ModelSerializer):
    created_time = serializers.DateTimeField(format='%Y-%m-%d')

    class Meta:
        model = Tag
        fields = '__all__'


class ArticleSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True)  # 指定序列化

    class Meta:
        model = Article
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    articles = ArticleSerializer(many=True)  # 指定序列化
    count = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('id', 'name', 'articles', 'count')

    @staticmethod
    def get_count(obj):  # 和属性的名字要一样 get_
        return obj.articles.count()


"""

source 重写返回字段


"""

#
# class MyCharField(serializers.CharField):
#     def to_representation(self, value):
#         data_list = []
#         for val in value:
#             data_list.append(
#                 {
#                     "id": val.id,
#                     "title": val.title,
#                     "vum": val.vum,
#                     "content": val.content
#                 }
#             )
#         return data_list
#
#
# class ArticleSerializer(serializers.ModelSerializer):
#     category = serializers.CharField(source='category.name')
#
#     class Meta:
#         model = Article
#         fields = '__all__'
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     # "arts": "<QuerySet [<Article: 文章标题>, <Article: 文章标题2222>, <Article: 111111文章标题>]>"
#     # 需要自定义属性 来 把 QuerySet转换为 json数据
#     arts = serializers.CharField(source='articles.all')
#     # arts = serializers.CharField(source='article_set.all') #如果没有related_name
#
#     arts = MyCharField(source='articles.all')  # 自定义 MyCharField
#
#     '''
#     get_xx_display
#     '''
#
#     class Meta:
#         model = Category
#         fields = ('id', 'name', 'arts')
#

"""
    to_representation
    
"""

#
# class ArticleSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Article
#         fields = '__all__'
#         # depth = 2
#
#     # 决定每个字段的返回值
#     def to_representation(self, instance):
#         representation = super(ArticleSerializer, self).to_representation(instance)
#         representation['category'] = CategorySerializer(instance.category).data
#         representation['tags'] = TagSerializer(instance.tags, many=True).data
#         tmp_tags = representation['tags']
#         tags_list = []
#         for i in tmp_tags:
#             print(i)
#             tags_list.append(i['name'])
#         print(tags_list)
#         representation['tags_all'] = ','.join(tags_list)
#         return representation
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Category
#         fields = ('id', 'name')
#
#
# class TagSerializer(serializers.ModelSerializer):
#     created_time = serializers.DateTimeField(format='%Y-%m-%d')
#
#     class Meta:
#         model = Tag
#         fields = '__all__'
