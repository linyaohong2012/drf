from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination, CursorPagination
from .custom_json_response import JsonResponse
from rest_framework import status


class CustomPageNumberPagination(PageNumberPagination):
    # 每页最大数量
    page_size = 10
    max_page_size = 20
    page_size_query_param = 'size'
    page_query_param = 'page'
    # http://127.0.0.1:8003/app06/v1/games/?page=1&size=2

    def get_paginated_response(self, data):
        return JsonResponse(data=data, code=200, msg="success", status=status.HTTP_200_OK, next=self.get_next_link(),
                            previous=self.get_previous_link(), count=self.page.paginator.count)

# class CustomPPageNumberPagination(LimitOffsetPagination):
#     default_limit = 10
#     max_limit = 20
#     limit_query_param = 'limit'
#     offset_query_param = 'offset'
    # http://127.0.0.1:8003/app06/v1/games/?limit=10&offset=10

#     def get_paginated_response(self, data):
#         return JsonResponse(data=data, code=200, msg="success", status=status.HTTP_200_OK, next=self.get_next_link(),
#                             previous=self.get_previous_link(), count=self.count)
#
#

# class CustomPPageNumberPagination(CursorPagination):
#     cursor_query_param = 'cursor'
#     page_size = 5
#     ordering = 'id'
#     page_size_query_param = 'size'
#     max_page_size = 10


#     def get_paginated_response(self, data):
#         return JsonResponse(data=data, code=200, msg="success", status=status.HTTP_200_OK, next=self.get_next_link(),
#                             previous=self.get_previous_link())
