# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/12/6 8:44 下午
# Filename: jwt_token_response.py
# Tools   : PyCharm


def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'id': user.id,
        'username': user.username,
        'last_login': user.last_login.strftime('%Y-%m-%d %H:%M:%S') if user.last_login else None,
        'firstname': user.first_name,
        'lastname': user.last_name,
        'is_staff': user.is_staff
    }
