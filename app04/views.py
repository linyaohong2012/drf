from django.shortcuts import render
from rest_framework import generics
from .models import Game
from .serializers import GameSerializer
from rest_framework import permissions
from .permissions import IsOwnOrReadOnly
from rest_framework import viewsets
from rest_framework.generics import GenericAPIView
from rest_framework import viewsets

# Create your views here.

# 通用的类视图

# class GameList(generics.ListCreateAPIView):
#     queryset = Game.objects.all()
#     serializer_class = GameSerializer
#
#     def perform_create(self, serializer):
#         serializer.save(user=self.request.user)
#         # 到底给request.user赋值的


# class GameDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Game.objects.all()
#     serializer_class = GameSerializer
#    permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnOrReadOnly]

from libs.customModelViewSet import CustomModelViewSet

from django_filters.rest_framework import DjangoFilterBackend
from .custom_filter import GameFilter
from rest_framework import filters


class GameViewSet(CustomModelViewSet):
    # class GameViewSet(viewsets.ModelViewSet):
    # authentication_classes = []
    # permission_classes = []
    # permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnOrReadOnly]
    permission_classes = []

    queryset = Game.objects.all()
    serializer_class = GameSerializer

    # 重写 user字段创建 取值  user
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        # 到底给request.user赋值的

    # # 使用自定义 get_queryset 来实现排序
    # def get_queryset(self):
    #     ordering = self.request.query_params.get('ordering')
    #     if not ordering:
    #         queryset = Game.objects.all()
    #     else:
    #         queryset = Game.objects.all().order_by(ordering)
    #     return queryset
    # # http://127.0.0.1:8003/api/v1/games/?ordering=id  // 排序方法 1

    # 过滤 不支持 模糊搜索
    # filter_backends = (DjangoFilterBackend,)  # // 局部
    # filter_fields = ('name', 'desc')

    # http://127.0.0.1:8003/api/v1/games/?name=老王
    # http://127.0.0.1:8003/api/v1/games/?desc=腾讯

    # 过滤 支持模糊搜索 模糊搜索
    # filter_backends = (DjangoFilterBackend,)   # // 局部
    # filterset_class = GameFilter

    # http://127.0.0.1:8003/api/v1/games/?name=老  // 支持模糊搜索
    # http://127.0.0.1:8003/api/v1/games/?min_status=8  // >= 8 的
    # http://127.0.0.1:8003/api/v1/games/?max_status=1  // <= 1
    # http://127.0.0.1:8003/api/v1/games/?min_status=8&max_status=9  >=8 <=9

    # 搜索  只要定义的字段都会搜索
    # filter_backends = (filters.SearchFilter,)  # 局部
    # search_fields = ('name', 'status', 'id',)

    # http://127.0.0.1:8003/api/v1/games/?search=刘

    # 排序
    filter_backends = (filters.OrderingFilter,)  # 局部
    # 支持排序的字段
    ordering_field = ('id', 'status')

    # http://127.0.0.1:8003/api/v1/games/?ordering=-id
    # http://127.0.0.1:8003/api/v1/games/?ordering=status
