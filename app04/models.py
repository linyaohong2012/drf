from django.db import models


# from django.contrib.auth.models import User, Permission, Group


class Game(models.Model):
    name = models.CharField(verbose_name='游戏名字', max_length=10)
    desc = models.CharField(verbose_name='描述', max_length=20)
    status = models.IntegerField(default=0, verbose_name='状态')
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='game')

    class Meta:
        ordering = ('-id',)
