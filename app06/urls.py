from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('games/<int:pk>/', views.GameDetail.as_view(), name='game-detail'),
    path('games/', views.GameList.as_view(), name='game-list'),
    path('parser/', views.ParserView.as_view(), name='parser'),
]

# 如果是 GameList.as_view() views.GameDetail.as_view()  在url里配置如下两行
# def put(self, request, format=None, *args, **kwargs):  // format=None,
# http://127.0.0.1:8003/app06/v1/games.json
# http://127.0.0.1:8003/app06/v1/games.api
# 如果是 ModelViewSet 则不需要加

from rest_framework.urlpatterns import format_suffix_patterns
urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])

