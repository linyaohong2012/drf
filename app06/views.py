from django.shortcuts import render
from rest_framework import generics
from .models import Games
from .serializers import GamesSerializer
from rest_framework import permissions
from .permissions import IsOwnOrReadOnly


# 主要测试分页
# Create your views here.

# 通用的类视图

class GameList(generics.ListCreateAPIView):
    queryset = Games.objects.all()
    serializer_class = GamesSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        # 到底给request.user赋值的


class GameDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Games.objects.all()
    serializer_class = GamesSerializer


from rest_framework.parsers import FormParser, JSONParser, FileUploadParser, MultiPartParser
from rest_framework.views import APIView
from django.http import HttpResponse


class ParserView(APIView):
    """
    parser_classes 默认支持  FormParser JSONParser MultiPartParser

    // from-data

    parser_classes = [MultiPartParser]
    body: ----------------------------535421446854749302168511
    Content - Disposition: form - data;
    name = "name"

    linyaohong
    ----------------------------535421446854749302168511 - -

    content_type: multipart / form - data;
    boundary = --------------------------535421446854749302168511
    data: < QueryDict: {'name': ['linyaohong']} >
    POST: < QueryDict: {'name': ['linyaohong']} >
    FILES: < MultiValueDict: {} >

    //  x-www-form-urlencoded

    parser_classes = [FormParser]
    body: name = linyaohong
    content_type: application / x - www - form - urlencoded
    data: < QueryDict: {'name': ['linyaohong']} >
    POST: < QueryDict: {'name': ['linyaohong']} >
    FILES: < MultiValueDict: {} >


    //JSONParser

    parser_classes = [JSONParser]

    body: {
        "name":"linyaohong"
    }
    content_type: application/json
    data: {'name': 'linyaohong'}
    POST: <QueryDict: {}>
    FILES: <MultiValueDict: {}>
    """

    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        print("body:", request.body.decode())
        print("content_type:", request.content_type)
        # 获取请求的值，并使用对应的JSONParser进行处理
        print("data:", request.data)
        # application/x-www-form-urlencoded 或 multipart/form-data时，request.POST中才有值
        print("POST:", request.POST)
        print("FILES:", request.FILES)

        return HttpResponse('响应')
