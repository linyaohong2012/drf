from rest_framework import serializers
from .models import Games
from django.forms.models import model_to_dict
from django.db.models.fields import DateTimeField
from django.db.models.fields.related import ManyToManyField


# Django model 转为字典
def class_model_to_dict(self, fields=None, exclude=None):
    data = {}
    for f in self._meta.concrete_fields + self._meta.many_to_many:
        value = f.value_from_object(self)
        if fields and f.name not in fields:
            continue
        if exclude and f.name in exclude:
            continue
        if isinstance(f, ManyToManyField):
            value = [i.id for i in value] if self.pk else None
        if isinstance(f, DateTimeField):
            value = value.strftime('%Y-%m-%d %H:%M:%S') if value else None
        data[f.name] = value

    return data


class GamesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Games
        fields = '__all__'
        # depth = 2
        extra_kwargs = {
            "user": {"read_only": True}
        }

    def to_representation(self, instance):
        representation = super(GamesSerializer, self).to_representation(instance)
        representation['user'] = class_model_to_dict(instance.user, fields=('id', 'username'))
        # representation['user'] = class_model_to_dict(instance.user, fields=('id', 'username', 'first_name',))

        return representation

    # def validate_name(self, name):
    #     if name == '吃鸡10':
    #         raise serializers.ValidationError('名字不合法')
    #
    # def validate_desc(self, desc):
    #
    #     if desc == '哈哈':
    #         raise serializers.ValidationError('描述不合法')
    #
    # def validate(self, attrs):
    #     raise serializers.ValidationError('无缘无故异常')
    #     return attrs
