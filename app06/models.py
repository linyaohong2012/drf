from django.db import models


# from django.contrib.auth.models import User, Permission, Group


class Games(models.Model):
    name = models.CharField(verbose_name='游戏名字', max_length=10)
    desc = models.CharField(verbose_name='描述', max_length=20)
    user = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='games')

    class Meta:
        ordering = ('-id',)
