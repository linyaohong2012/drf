# -*- coding:UTF-8 -*-
# Author  : LinYaoHong
# Date    : 2020/12/2 10:22 上午
# Filename: permissions.py
# Tools   : PyCharm

from rest_framework import permissions


class IsOwnOrReadOnly(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.method in permissions.SAFE_METHODS:  # 是否在安全方法内
            return True

        return obj.user == request.user