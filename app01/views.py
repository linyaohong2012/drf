from rest_framework import viewsets
from .serializers import *


class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()      # 要序列化的数据
    serializer_class = GroupSerializer  # 要是哪个序列化类
