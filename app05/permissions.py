from rest_framework.permissions import BasePermission

from rest_framework.authentication import BaseAuthentication
from .models import UserToken
from rest_framework import exceptions


#  自定义token认证
class MyAuthentication(BaseAuthentication):
    def authenticate(self, request):
        token = request.META.get('HTTP_TOKEN')
        obj = UserToken.objects.filter(token=token).first()
        if not obj:
            raise exceptions.AuthenticationFailed('验证失败')
        else:
            return obj.user, obj


# 自定义权限
class MyPermission(BasePermission):

    def has_permission(self, request, view):
        if not request.user:
            return False

        return True
