from django.shortcuts import render
from rest_framework.views import APIView
from django.http import JsonResponse

from rest_framework.authentication import (
    BaseAuthentication,
    BasicAuthentication,
    TokenAuthentication,
    SessionAuthentication
)
from rest_framework.permissions import IsAuthenticated
from .models import User, UserToken
from .throttlings import VisitThrottling
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
# Create your views here.
import time
import hashlib
from django.http import HttpResponse
from .permissions import *


# Drf 默认认证方式 两种
# 'rest_framework.authentication.SessionAuthentication',   # 基于 Session认证
# 'rest_framework.authentication.BasicAuthentication'      # 基于 浏览器账号密码认证
# 支持多个认证, 取默认认真成功的
# 返回2个值 user 复制给 request

# 基于token认证 TokenAuthentication


def get_md5(user):
    ctime = str(time.time())
    m = hashlib.md5(bytes(user, encoding='utf-8'))
    m.update(bytes(ctime, encoding='utf-8'))
    return m.hexdigest()


class LoginView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        ret = {'code': 1, 'msg': None, 'data': {}}
        # user = request._request.POST.get('username')
        # user = request._request.POST.get('username')
        user = request.POST.get('username')
        pwd = request.POST.get('password')
        obj = User.objects.filter(username=user, password=pwd).first()
        if not obj:
            ret['code'] = -1
            ret['msg'] = "用户名或密码错误"
        token = get_md5(user)
        UserToken.objects.update_or_create(user=obj, defaults={'token': token})
        ret['token'] = token
        return JsonResponse(ret)


# JWT验证
# 使用django-rest-framework开发api并使用json web token进行身份验证,使用django-rest-framework-jwt这个库来帮助我们简单的使用jwt进行身份验证。
# pip install djangorestframework-jwt
# 注册

# INSTALLED_APPS = [
#     ''''''
#     'rest_framework',
#     'rest_framework.authtoken',
# ]

class CartView(APIView):
    #  都是局部的

    # 基于什么登录认证的
    # authentication_classes = [BasicAuthentication, TokenAuthentication, SessionAuthentication]

    # 自己写的认证类
    # authentication_classes = [MyAuthentication]

    # 基于jwt验证
    # authentication_classes = [JSONWebTokenAuthentication]

    # 只有登录才能访问
    # permission_classes = [IsAuthenticated]

    # 自己的权限
    # permission_classes = [MyPermission]

    # 节流 局部 空为不限制
    # throttle_classes = [VisitThrottling]

    def get(self, request, *args, **kwargs):
        return JsonResponse({"code": 0})

    def put(self, request, *args, **kwargs):
        return HttpResponse('ok')


from rest_framework.versioning import BaseVersioning


class VersionView(APIView):
    def get(self, request, *args, **kwargs):
        print(request.version)

        if request.version == 'v1':
            ctx = {"code": 1, "msg": "ok", "data": {}}
            return JsonResponse(ctx)
        else:
            ctx = {"code": 2, "msg": "ok", "data": {}}
            return JsonResponse(ctx)

